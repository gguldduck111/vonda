// Imports
import Vue from 'vue'
import Router from 'vue-router'
import { trailingSlash } from '@/util/helpers'
import {
  layout,
  route,
} from '@/util/routes'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  scrollBehavior: (to, from, savedPosition) => {
    if (to.hash) return { selector: to.hash }
    if (savedPosition) return savedPosition

    return { x: 0, y: 0 }
  },
  routes: [
    layout('Default', [
      route('Dashboard'),

      // Pages
      route('UserProfile', null, 'components/profile','광고 등록'),

      // Components
      route('Notifications', null, 'components/notifications','광고 등록'),
      route('Icons', null, 'components/icons','광고 등록'),
      route('Typography', null, 'components/typography','광고 등록'),

      // Tables
      route('Regular Tables', null, 'tables/regular','광고 등록'),

      // Maps
      route('Google Maps', null, 'maps/google','광고 등록'),
      route('CreateCampaign', null, '/campaign/add','광고 등록'),

    ]),
  ],
})

router.beforeEach((to, from, next) => {
  return to.path.endsWith('/') ? next() : next(trailingSlash(to.path))
})

export default router
