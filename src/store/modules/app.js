// Pathify
import { make } from 'vuex-pathify'

// Data
const state = {
  drawer: null,
  drawerImage: true,
  mini: false,
  items: [
    {
      title: '광고',
      icon: 'mdi-view-dashboard',
      to: '/',
    },
    {
      title: '보고서',
      icon: 'mdi-chart-timeline-variant',
      to: '/components/profile/',
    },
    {
      title: '정산',
      icon: 'mdi-application',
      to: '/calculate',
    },
    {
      title: '관리',
      icon: 'mdi-auto-fix',
      to: '/history',
    }

  ],
}

const mutations = make.mutations(state)

const actions = {
  ...make.actions(state),
  init: async ({ dispatch }) => {
    //
  },
}

const getters = {}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
}
